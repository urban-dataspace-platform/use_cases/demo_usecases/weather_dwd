
# Demo Use-Case DWD

## Projektbeschreibung

In diesem Use-Case wurde die Open Data API vom [Deutschen Wetterdienst](https://dwd.api.bund.dev/) eingebunden. Die API liefert Wetterdaten und -vorhersagen von den Wetterstationen des DWD.

Der Use-Case wurde auf der FIWARE-basierten [Urban Dataspace Platform](https://gitlab.com/urban-dataspace-platform/core-platform) entwickelt und integriert.

## Installation

Für die Installation und Konfiguration dieses Use-Cases wird auf das [deployment repository](https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/deployment) verwiesen

## Funktionsweise

Der NodeRED-Flow ruft die Daten der konfigurierten Station per HTTP-Request von der API ab. Diese Daten werden in die NGSI-kompatiblen [WeatherObserved](https://github.com/smart-data-models/dataModel.Weather/blob/master/WeatherObserved/README.md) und [WeatherForecast](https://github.com/smart-data-models/dataModel.Weather/blob/master/WeatherForecast/README.md) Smart-Data-Models umgewandelt und an die Datenplattform gesendet. 


## Kontaktinformationen

Dieses Projekt wurde von der [Hypertegrity AG](https://www.hypertegrity.de/) entwickelt.

## Link zum Original-Repository

https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/weather_dwd